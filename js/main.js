//nextLandmarkLabel
//nextLandmarkCountdown


var hackathonStarts = new Date('2015/03/13 20:00');
var hackathonEnds = new Date('2015/03/15 10:00');
/*
var hackathonStarts = new Date();
hackathonStarts.setSeconds(hackathonStarts.getSeconds() + 30);

var hackathonEnds = new Date();
hackathonEnds.setSeconds(hackathonEnds.getSeconds() + 120);
*/
var landmarkLabels = Array('Arrivals', 'Teams', 'Launch', 'Pizza 1', 'Pizza 2', 'Pizza 3', 'Midnight','Breakfast', 'Pizza 1', 'Pizza 2', 'Pizza 3', 'Midnight','Breakfast', 
  'Present', 'Judging');
var landmarkDates = Array(new Date('2015/03/13 18:00'),new Date('2015/03/13 19:00'),new Date('2015/03/13 20:00'),
  new Date('2015/03/13 20:30'),new Date('2015/03/13 21:30'),new Date('2015/03/13 22:30'),new Date('2015/03/14 00:00'),
  new Date('2015/03/14 08:00'),new Date('2015/03/14 20:30'),new Date('2015/03/14 20:30'),new Date('2015/03/14 20:30'),
  new Date('2015/03/14 00:00'), new Date('2015/03/15 08:00'),new Date('2015/03/15 10:00'),new Date('2015/03/15 11:00'));

/*var landmarkDates = Array(new Date(),new Date(),new Date(),new Date(),new Date(),new Date(),new Date(),new Date(),new Date(),new Date());
var now = Date.now();
for(var i=0;i<landmarkDates.length;i++)
{

  landmarkDates[i] = new Date(now + 40000 + (i*10000));
}*/

$(document).ready(function()
{
  setUpMainCountdown();
  showLandmarkCountdown();

    // update on finish
    $('#totalCountdown').on('countdownEnd', function() {
        setUpMainCountdown();
    });

    // update on finish
    $('#nextLandmarkCountdown').on('countdownEnd', function() {
        showLandmarkCountdown();
    });
});

var mainCountdown;

function setUpMainCountdown()
{
  if(new Date() < hackathonStarts)
  {
    $('#totalTimeLabel').text('Start: ');

    mainCountdown = new Countdown({
        selector: '#totalCountdown',
        msgPattern: "{hours}:{minutes}:{seconds}",
        //dateStart:  new Date('2014/09/26 19:00'),
        dateEnd: hackathonStarts,
        msgBefore: 'Get Ready!',
        msgAfter: 'Complete',
        leadingZeros: true
    });

  }
  else
  {
    $('#totalTimeLabel').text('Finish: ');

    mainCountdown = new Countdown({
        selector: '#totalCountdown',
        msgPattern: "{hours}:{minutes}:{seconds}",
        //dateStart:  new Date('2014/09/26 19:00'),
        dateEnd: hackathonEnds,
        msgBefore: 'Get Ready!',
        msgAfter: 'Game Over!',
        leadingZeros: true
    });
  }

  showLandmarkCountdown();
}

var landmarkCountdown;
function showLandmarkCountdown()
{
    var label = '';
  var date = Date.now();
  var now = date;

  for(var i=0;i<landmarkLabels.length;i++)
  {
    if(landmarkDates[i] > now)
    {
      label=landmarkLabels[i];
      date=landmarkDates[i];
      break;
    }
  }

  if(label != '') // && hackathonStarts.getTime() < now)
  {
    $('#nextLandmarkLabel').text(label + ': ');

    landmarkCountdown = new Countdown({
        selector: '#nextLandmarkCountdown',
        msgPattern: "{hours}:{minutes}:{seconds}",
        //dateStart:  new Date('2014/09/26 19:00'),
        dateEnd: date,
        msgBefore: 'Get Ready!',
        msgAfter: 'Complete',
        leadingZeros: true
    });

  }
  else
  {
    $('#nextLandmarkLabel').text('...');
    $('#nextLandmarkCountdown').text('...');
  }
}